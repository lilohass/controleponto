//
//  AddPontoManualViewController.m
//  ControlePonto
//
//  Created by William Hass on 3/22/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "AddPontoManualViewController.h"
#import "PontoModel.h"

@interface AddPontoManualViewController () {
    UIDatePicker *_datePicker;
    UILabel *_lblHorario;
    UIButton *_btnMarcar;
    UIButton *_btnDesfazer;
    UILabel *_lblPontoMarcadoMsg;
    UILabel *_lblUltimoPontoMarcado;
    PontoModel *_pontoModel;
    Ponto *_ultimoPontoRegistrado;
    NSDateFormatter *_formatter;
    //ADBannerView *_iAd;
    GADBannerView *_gAd;
    UIView *_adView;
}

@end

@implementation AddPontoManualViewController


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self hideElements];
    
    // Toolbar
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    _adView         = [[UIView alloc] init];
    _adView.frame   = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden  = YES;
    
    // iAd
    /*_iAd            = [[ADBannerView alloc] init];
     //_iAd.delegate   = self;
     _iAd.hidden     = YES;
     _iAd.frame      = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
     
     [_adView addSubview:_iAd];*/
    
    _gAd = [[GADBannerView alloc] init];
    _gAd.frame              = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
    _gAd.adUnitID           = @"ca-app-pub-6347221915460502/2744027679";
    _gAd.rootViewController = self;
    _gAd.delegate           = self;
    
    
    
    GADRequest *gadRequest = [GADRequest request];
    gadRequest.testDevices = @[GAD_SIMULATOR_ID, DEVICE_TEST_1];
    
    [_gAd loadRequest:gadRequest];
    
    [_adView addSubview:_gAd];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_adView removeFromSuperview];
    _adView = nil;
    _gAd = nil;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Inserir ponto manual";
    
    self.view.backgroundColor = [UIColor appBlueColor];
    
    _pontoModel = [[PontoModel alloc] init];
    
    _formatter = [[NSDateFormatter alloc] init];
    [_formatter setDateFormat:@"dd/MM/yyyy HH:mm"];
    
    [self createElements];
}


-(void)createElements
{
    // Declara Date Picker
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.locale          = [NSLocale localeWithLocaleIdentifier:@"pt-BR"];
    _datePicker.date            = [NSDate date];
    _datePicker.datePickerMode  = UIDatePickerModeDateAndTime;
    [_datePicker addTarget:self action:@selector(dateChanged:) forControlEvents:UIControlEventAllEvents];
    [_datePicker sizeToFit];
    
    // Label com horario
    CGFloat lblHorarioY         = self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height;
    _lblHorario = [[UILabel alloc] initWithFrame:CGRectMake(0, lblHorarioY, self.view.frame.size.width, 20)];
    _lblHorario.textAlignment   = NSTextAlignmentCenter;
    _lblHorario.font            = [UIFont fontWithName:@"Helvetica" size:24];
    _lblHorario.textColor       = [UIColor lightTextColor];
    
    [self.view addSubview:_lblHorario];
    // Atualiza data
    [self dateChanged:nil];
    
    // Seta Frame date picker
    CGFloat datePickerY = lblHorarioY+_lblHorario.frame.size.height + 5;
    _datePicker.frame   = CGRectMake(0, datePickerY, _datePicker.frame.size.width, _datePicker.frame.size.height);
    
    [self.view addSubview:_datePicker];
    
    
    // Label Marcado
    CGFloat lblPontoMarcadoY    = datePickerY + _datePicker.frame.size.height;
    _lblPontoMarcadoMsg         = [[UILabel alloc] init];
    _lblPontoMarcadoMsg.frame   = CGRectMake(10, lblPontoMarcadoY, _datePicker.frame.size.width - 10 , 20);
    _lblPontoMarcadoMsg.font    = [UIFont fontWithName:@"Helvetica Bold" size:15];
    _lblPontoMarcadoMsg.alpha   = 0;
    
    [self.view addSubview:_lblPontoMarcadoMsg];
    
    // Label Ultimo Ponto Marcado
    _lblUltimoPontoMarcado              = [[UILabel alloc] init];
    _lblUltimoPontoMarcado.font         = [UIFont fontWithName:@"Helvetica" size:16];
    _lblUltimoPontoMarcado.text         = @"99/99/9999 99:99";
    _lblUltimoPontoMarcado.textColor    = [UIColor lightTextColor];
    _lblUltimoPontoMarcado.alpha        = 0;
    _lblUltimoPontoMarcado.frame = CGRectMake(_lblPontoMarcadoMsg.frame.origin.x,
                                              _lblPontoMarcadoMsg.frame.origin.y + _lblPontoMarcadoMsg.frame.size.height + 5,
                                              _lblPontoMarcadoMsg.frame.size.width,
                                              _lblPontoMarcadoMsg.frame.size.height);
    
    [self.view addSubview:_lblUltimoPontoMarcado];
    
    
    // Btn Desfazer
    _btnDesfazer                    = [[UIButton alloc] init];
    _btnDesfazer.titleLabel.font    = [UIFont fontWithName:@"Helvetica" size:15];
    _btnDesfazer.alpha              = 0;
    
    [_btnDesfazer setTitle:@"Desfazer" forState:UIControlStateNormal];
    [_btnDesfazer setTitleColor:[UIColor appRedColor] forState:UIControlStateNormal];
    [_btnDesfazer setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [_btnDesfazer setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_btnDesfazer addTarget:self action:@selector(btnDesfazerTouched:) forControlEvents:UIControlEventTouchUpInside];

    _btnDesfazer.frame = CGRectMake(_lblUltimoPontoMarcado.frame.origin.x,
                                    _lblUltimoPontoMarcado.frame.origin.y + _lblUltimoPontoMarcado.frame.size.height,
                                    62,
                                    28);
    
    [self.view addSubview:_btnDesfazer];
    
    // Btn Marcar
    CGFloat btnMarcarHeight = 40;
    _btnMarcar = [[UIButton alloc] init];
    _btnMarcar.frame            = CGRectMake(0, (self.view.frame.size.height-50) - btnMarcarHeight - 2 , _datePicker.frame.size.width, btnMarcarHeight);
    _btnMarcar.backgroundColor  = [UIColor lightTextColor];
    
    [_btnMarcar setTitle:@"Marcar" forState:UIControlStateNormal];
    [_btnMarcar setTitleColor:[UIColor appBlueColor] forState:UIControlStateNormal];
    [_btnMarcar setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [_btnMarcar addTarget:self action:@selector(btnMarcarTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnMarcar];
}


- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if([anim.description isEqualToString:@"fade-out-marcar"])
        [self fadeInElementosMarcar];
    else if([anim.description isEqualToString:@"fade-out-desfazer"])
        [self fadeInElementosDesfazer];
}


- (void)fadeInElementosDesfazer
{
    [UIView beginAnimations:@"fade-in-marcar" context:nil];
    [UIView setAnimationDuration:0.3];
    
    [self updateLabelUltimoPontoMarcado];
    _ultimoPontoRegistrado = nil;
    
    _lblPontoMarcadoMsg.text            = @"Ponto excluído com sucesso!";
    _lblPontoMarcadoMsg.textColor       = [UIColor appRedColor];
    _lblUltimoPontoMarcado.textColor    = [UIColor appRedColor];
    
    [self showElements];
    
    [UIView commitAnimations];
}

- (void)fadeInElementosMarcar
{
    [UIView beginAnimations:@"fade-in-marcar" context:nil];
    [UIView setAnimationDuration:0.3];
    
    [self updateLabelUltimoPontoMarcado];
    
    _lblPontoMarcadoMsg.text            = @"Ponto registrado com sucesso!";
    _lblPontoMarcadoMsg.textColor       = [UIColor lightTextColor];
    _lblUltimoPontoMarcado.textColor    = [UIColor lightTextColor];
    
    [self showElements];
    
    _btnDesfazer.hidden             = NO;
    _btnDesfazer.alpha              = 1;
    
    [UIView commitAnimations];
}


- (void)fadeOutElementos:(NSString *)ident
{
    [UIView beginAnimations:ident context:nil];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    
    [self hideElements];
    
    [UIView commitAnimations];
}


- (void)hideElements
{
    _lblPontoMarcadoMsg.alpha       = 0;
    _btnDesfazer.alpha              = 0;
    _lblUltimoPontoMarcado.alpha    = 0;
}


- (void)showElements
{
    _lblUltimoPontoMarcado.alpha    = 1;
    _lblPontoMarcadoMsg.alpha       = 1;
}



- (void)btnMarcarTouched:(id)sender
{
    _ultimoPontoRegistrado      = [[Ponto alloc] init];
    _ultimoPontoRegistrado.data = [_datePicker.date timeIntervalSince1970];
    [_pontoModel salvarPonto:_ultimoPontoRegistrado];
    
    [self fadeOutElementos:@"fade-out-marcar"];
}

- (void)btnDesfazerTouched:(id)sender
{
    [_pontoModel excluirPonto:_ultimoPontoRegistrado];
    
    [self fadeOutElementos:@"fade-out-desfazer"];
}


- (void)updateLabelUltimoPontoMarcado
{
    _lblUltimoPontoMarcado.text = [NSString stringWithFormat:@"- %@",[_formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970: _ultimoPontoRegistrado.data]]];
}


- (void)dateChanged:(id)sender
{
    _lblHorario.text = [_formatter stringFromDate:_datePicker.date];
}


#pragma mark Google Ad View Banner Delegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    [self showAdView];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    [self hideAdView];
}


- (void)showAdView
{
    if(!_adView.superview)
        [self.view addSubview:_adView];
    
    if(_adView.hidden) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        _adView.hidden             = NO;
        _adView.frame              = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50);
        _adView.backgroundColor = [UIColor redColor];
        
        [UIView commitAnimations];
    }
}


- (void)hideAdView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _adView.frame = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden = YES;
    
    [UIView commitAnimations];
}


@end
