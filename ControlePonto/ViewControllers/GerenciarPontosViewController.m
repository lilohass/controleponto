//
//  GerenciarPontosViewController.m
//  ControlePonto
//
//  Created by William Hass on 3/4/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "GerenciarPontosViewController.h"

typedef enum {
    GrupoTabelaDia,
    GrupoTabelaMes,
    GrupoTabelaAno
} GrupoTabela;

typedef enum {
    OrdemTabelaAsc,
    OrdemTabelaDesc
} OrdemTabela;

@interface GerenciarPontosViewController () {
    UIBarButtonItem *_btnEdit;
    NSDictionary *_grupos;
    NSMutableArray *_sectionKeys;
    GrupoTabela agrupamento;
    OrdemTabela ordemTabela;
    CGRect tableFrame;
    //ADBannerView *_iAd;
    GADBannerView *_gAd;
    UIView *_adView;
}
@end

@implementation GerenciarPontosViewController

static GrupoTabela grupoDefault = GrupoTabelaMes;
static OrdemTabela ordemDefault = OrdemTabelaDesc;

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self loadTableData];
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    tableFrame = self.tableView.frame;
    
    _adView         = [[UIView alloc] init];
    _adView.frame   = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden  = YES;
    
    /* iAd
    _iAd            = [[ADBannerView alloc] init];
    _iAd.delegate   = self;
    _iAd.hidden     = YES;
    _iAd.frame      = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
     
    [_adView addSubview:_iAd];*/
    
    _gAd = [[GADBannerView alloc] init];
    _gAd.frame              = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
    _gAd.adUnitID           = @"ca-app-pub-6347221915460502/1267294479";
    _gAd.rootViewController = self;
    _gAd.delegate           = self;
    
    
    
    GADRequest *gadRequest = [GADRequest request];
    gadRequest.testDevices = @[GAD_SIMULATOR_ID, DEVICE_TEST_1];
    
    [_gAd loadRequest:gadRequest];
    
    [_adView addSubview:_gAd];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_adView removeFromSuperview];
    _adView = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor appBlueColor];
    
    self.title = @"Gerenciar pontos";
    
    // Model
    _modelPonto = [[PontoModel alloc] init];
    
    // Botao Editar
    _btnEdit = [[UIBarButtonItem alloc] init];
    [_btnEdit setTarget:self];
    [_btnEdit setAction:@selector(btnEditTouched:)];
    
    // Grupo Padrao
    agrupamento = grupoDefault;
    
    // Ordem
    ordemTabela = ordemDefault;
    
    [self loadTableData];
    
    self.clearsSelectionOnViewWillAppear = NO;
    
    self.tableView.allowsSelection = YES;
    
    self.navigationItem.rightBarButtonItem = _btnEdit;
    
    
    
    UIBarButtonItem    *btnOrdenar = [[ UIBarButtonItem alloc ] initWithTitle: @"Ordenar"
                                                    style: UIBarButtonItemStyleBordered
                                                   target: self
                                                   action: @selector(btnOrdenarTouched:)];
    
    UIBarButtonItem    *btnAgrupar = [[ UIBarButtonItem alloc ] initWithTitle: @"Agrupar"
                                                                        style: UIBarButtonItemStyleBordered
                                                                       target: self
                                                                       action: @selector(btnAgruparTouched:)];
    
    UIBarButtonItem *spacer = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    self.toolbarItems = [ NSArray arrayWithObjects: btnAgrupar, spacer, btnOrdenar, nil];
}


- (void)setTextoBtnEdit:(BOOL)isEditing
{
    if(isEditing)
        _btnEdit.title = @"Cancelar";
    else
        _btnEdit.title = @"Editar";
}


- (void)btnOrdenarTouched:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Ordem" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Crescente",@"Decrescente", nil];
    sheet.tag = 1;

    [sheet showInView:self.navigationController.view];
}

- (void)btnAgruparTouched:(id)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Grupo" delegate:self cancelButtonTitle:@"Cancelar" destructiveButtonTitle:nil otherButtonTitles:@"Dia", @"Mes",@"Ano", nil];
    sheet.tag = 0;
    [sheet showInView:self.navigationController.view];
}


- (void)btnEditTouched:(id)sender
{
    if(_tableData.count <= 0)
        return;
    else if(!self.tableView.isEditing)
        [self.tableView setEditing:YES animated:YES];
    else
        [self.tableView setEditing:NO animated:YES];
    
    [self setTextoBtnEdit:self.tableView.isEditing];
}


- (void)loadTableData
{
    NSString *ordem;
    
    if(ordemTabela == OrdemTabelaAsc)
        ordem = @"ASC";
    else if(ordemTabela == OrdemTabelaDesc)
        ordem = @"DESC";
    
    _tableData = [_modelPonto getPontosWithOrder:ordem];
    
    // Mostra ou Esconte Toolbar
    if(_tableData.count <= 0)
        [self.navigationController setToolbarHidden:YES animated:YES];
    else
        [self.navigationController setToolbarHidden:NO animated:YES];
    
    [self setTextoBtnEdit:!(!self.tableView.isEditing || _tableData.count <= 0)];
    
    // Habilita ou desabilita botão
    if(_tableData.count == 0) {
        _btnEdit.enabled = NO;
        return;
    } else
        _btnEdit.enabled = YES;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"pt-BR"]];

    if(agrupamento == GrupoTabelaAno)
        [formatter setDateFormat:@"yyyy"];
    else if(agrupamento == GrupoTabelaMes)
        [formatter setDateFormat:@"MMMM/yyyy"];
    else if(agrupamento == GrupoTabelaDia)
        [formatter setDateFormat:@"dd/MMMM"];
    
    // Monta Grupos
    NSMutableDictionary *grupos = [[NSMutableDictionary alloc] init];
    
    _sectionKeys = [[NSMutableArray alloc] init];
    
    for(Ponto *p in _tableData) {
        NSString *indice = [[formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:p.data]] capitalizedString];
        
        BOOL indiceExiste = NO;
        for(NSString *indice2 in _sectionKeys) {
            if([indice2 isEqualToString:indice]) {
                indiceExiste = YES;
                break;
            }
        }
        
        if(!indiceExiste)
            [_sectionKeys addObject:indice];
        
        if([grupos objectForKey:indice] == nil)
            [grupos setObject:[[NSMutableArray alloc] init] forKeyedSubscript:indice];
        
        
        [[grupos objectForKey:indice] addObject:p];
    }
    
    _grupos = [NSDictionary dictionaryWithDictionary:grupos];
    
}

#pragma mark - Table view data source

- (NSString *)tableView:(UITableView *)tableView titleForFooterInSection:(NSInteger)section
{
    NSString *key   = [_sectionKeys objectAtIndex:section];
    NSArray *items  = [_grupos objectForKey:key];
    
    
    NSEnumerator *itemsEnum = (ordemTabela == OrdemTabelaDesc ? [items reverseObjectEnumerator] : [items objectEnumerator] );
    
    
    // Inverte Ordem
    Ponto *pInicial;
    Ponto *pFinal;
    double acumulado = 0;
    while (pInicial = [itemsEnum nextObject]) {
        
        // Chegou ao fim!
        if(!(pFinal = [itemsEnum nextObject]))
            break;
        
        acumulado += (pFinal.data - pInicial.data);
    }
    
    NSString *horas = [NSDate formataStrSegundos:acumulado];
    
    return [NSString stringWithFormat:@"Total de horas no período: %@", horas];
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    NSString *ret;
    NSString *key = [_sectionKeys objectAtIndex:section];
    
    if(agrupamento == GrupoTabelaAno)
        ret = [NSString stringWithFormat:@"Ano - %@",key];
    else if(agrupamento == GrupoTabelaDia)
        ret = [NSString stringWithFormat:@"Dia - %@",key];
    else if(agrupamento == GrupoTabelaMes)
        ret = [NSString stringWithFormat:@"Mes - %@",key];
    
    return ret;
}


- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return @"Excluir";
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _sectionKeys.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *key = [_sectionKeys objectAtIndex:section];
    return [[_grupos objectForKey:key] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor lightTextColor];
    }
    
    NSString *key = [_sectionKeys objectAtIndex:indexPath.section];
    Ponto *p = [[_grupos objectForKey:key] objectAtIndex:indexPath.row];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/Y HH:mm:ss"];
    
    
    cell.textLabel.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:p.data]];
    
    return cell;
}


- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        NSString *key = [_sectionKeys objectAtIndex:indexPath.section];
        
        Ponto *p = [[_grupos objectForKey:key] objectAtIndex:indexPath.row];
        [_modelPonto excluirPonto:p];
        
        [[_grupos objectForKey:key] removeObjectAtIndex:indexPath.row];
        
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
    }
}



#pragma mark Table View Delegate

/*- (void)tableView:(UITableView *)tableView willDisplayFooterView:(UIView *)view forSection:(NSInteger)section
{
}*/


- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"footer"];
    header.textLabel.textColor = [UIColor lightTextColor];
    header.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    header.contentView.backgroundColor = [UIColor lightGrayColor];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 22, 320, 1)];
    line.backgroundColor = [UIColor appBlueColor];
    [header.contentView addSubview:line];
    
    return header;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:@"header"];
    header.textLabel.textColor = [UIColor appBlueColor];
    header.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    header.contentView.backgroundColor = [UIColor whiteColor];
    return header;
}

- (void)tableView:(UITableView*)tableView willBeginEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setTextoBtnEdit:YES];
}


- (void)tableView:(UITableView*)tableView didEndEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self setTextoBtnEdit:NO];
}

#pragma mark Action Sheet Delegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    // Agrupar
    if(actionSheet.tag == 0) {
        
        if(buttonIndex == 0)
            agrupamento = GrupoTabelaDia;
        else if(buttonIndex == 1)
            agrupamento = GrupoTabelaMes;
        else if(buttonIndex == 2)
            agrupamento = GrupoTabelaAno;
        
        if(buttonIndex >= 0 && buttonIndex <= 2) {
            [self loadTableData];
            [self.tableView reloadData];
        }
        
        
    }
    // Ordenar
    else if(actionSheet.tag == 1) {
        if(buttonIndex == 0)
            ordemTabela = OrdemTabelaAsc;
        if(buttonIndex == 1)
            ordemTabela = OrdemTabelaDesc;
        
        if(buttonIndex == 0 || buttonIndex == 1) {
            [self loadTableData];
            [self.tableView reloadData];
        }
    }
}

#pragma mark Ad Banner View Delegate
/*- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _iAd.frame              = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _iAd.hidden             = YES;
    
    CGFloat bottom = (self.navigationController.toolbarHidden ? 0 : self.navigationController.toolbarHidden);
    self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, self.tableView.contentInset.left, bottom, self.tableView.contentInset.right);
    
    [UIView commitAnimations];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if(!_iAd.superview) {
        [self.view.superview addSubview:_iAd];
    }
    
    
    if(_iAd.hidden) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGFloat iadHeight       = self.view.frame.size.height-50 - (self.navigationController.toolbarHidden ? 0 : self.navigationController.toolbar.frame.size.height);
        
        _iAd.hidden             = NO;
        _iAd.frame              = CGRectMake(0, iadHeight, self.view.frame.size.width, 50);
        
        CGFloat bottom = _iAd.frame.size.height + (self.navigationController.toolbarHidden ? 0 : self.navigationController.toolbar.frame.size.height);
        self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, self.tableView.contentInset.left, bottom, self.tableView.contentInset.right);
        
        [UIView commitAnimations];
    }
    
}
- (void)bannerViewWillLoadAd:(ADBannerView *)banner
{
}*/

#pragma mark Google Ad View Banner Delegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    [self showAdView];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    [self hideAdView];
}


- (void)showAdView
{
    if(!_adView.superview)
        [self.view.superview addSubview:_adView];
    
    if(_adView.hidden) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        CGFloat iadHeight       = self.view.frame.size.height-50 - (self.navigationController.toolbarHidden ? 0 : self.navigationController.toolbar.frame.size.height);
        
        _adView.hidden             = NO;
        _adView.frame              = CGRectMake(0, iadHeight, self.view.frame.size.width, 50);
        
        CGFloat bottom = _adView.frame.size.height + (self.navigationController.toolbarHidden ? 0 : self.navigationController.toolbar.frame.size.height);
        self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, self.tableView.contentInset.left, bottom, self.tableView.contentInset.right);
        
        [UIView commitAnimations];
    }
}


- (void)hideAdView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _adView.frame = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden = YES;
    
    CGFloat bottom = (self.navigationController.toolbarHidden ? 0 : self.navigationController.toolbarHidden);
    self.tableView.contentInset = UIEdgeInsetsMake(self.tableView.contentInset.top, self.tableView.contentInset.left, bottom, self.tableView.contentInset.right);
    
    [UIView commitAnimations];
}

@end
