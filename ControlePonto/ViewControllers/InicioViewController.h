//
//  InicioViewController.h
//  ControlePonto
//
//  Created by William Hass on 2/18/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InicioViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, ADBannerViewDelegate, GADBannerViewDelegate>


- (void)btnMarcarHorarioTouched:(id)sender;
- (void)btnDesfazerTouched:(id)sender;

@property (strong, nonatomic) UIButton *btnMarcarHorario;
@property (strong, nonatomic) UILabel *lblUltimoPonto;
@property (strong, nonatomic) UILabel *lblRelogio;
@property (strong, nonatomic) UIButton *btnDesfazer;
@property (strong, nonatomic) UILabel *lblUltimoPontoTexto;

@end
