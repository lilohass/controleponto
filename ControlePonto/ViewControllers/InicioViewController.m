//
//  InicioViewController.m
//  ControlePonto
//
//  Created by William Hass on 2/18/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "InicioViewController.h"
#import "PontoModel.h"
#import "UIViewController+JASidePanel.h"

@interface InicioViewController ()
{
    PontoModel *modelPonto;
    Ponto *ultimoPontoRegistrado;
    int marcadorSelecionado;
    UITableView *table;
    NSArray *tableData;
    //ADBannerView *_iAd;
    GADBannerView *_gAd;
    UIView *_adView;
    NSDate *_inicioDebug;
    NSOperationQueue *_queue;
    
}
@end

@implementation InicioViewController

static NSString* escondeLabelAnimationID = @"esconde_ultimoponto";
static NSString* dateFormatComplete = @"dd/MM/yyyy HH:mm:ss";
static CGFloat verticalSpace = 15;
static int maxTableItems = 6;


- (id) initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if(self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"pt-BR"]];
        
        // Dia Mes
        [formatter setDateFormat:@"dd"];
        NSString *numeroDia = [formatter stringFromDate:[NSDate date]];
        
        // Nome Mes
        [formatter setDateFormat:@"MMMM"];
        NSString *mes = [formatter stringFromDate:[NSDate date]];
        
        // Title
        self.title = [NSString stringWithFormat:@"%@ - %@",numeroDia, [mes capitalizedString]];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Debug Tempo
    _inicioDebug = [NSDate date];
    
    // Model
    modelPonto = [[PontoModel alloc] initWithDebug:NO];
    
    // Botoes
    [self addButtons];
    
    // Table
    [self customizeTableView];
    
    // Tarefas
    [NSTimer scheduledTimerWithTimeInterval:0.5  target:self selector:@selector(timerTarefas:) userInfo:nil repeats:YES];
    
    // View
    self.view.backgroundColor = [UIColor appBlueColor];
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // Dados
    [self loadTableData:YES];
    [self loadUltimoPontoRegistrado];
    
    // Atuliza Ultimo Ponto
    [self atualizaTempoUltimoPonto];
    
    // Atualiza Relogio
    [self atualizaRelogio];
    
    // Toolbar
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    _adView         = [[UIView alloc] init];
    _adView.frame   = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden  = YES;

    /* iAd
    _iAd            = [[ADBannerView alloc] init];
    _iAd.delegate   = self;
    _iAd.hidden     = YES;
    _iAd.frame      = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
    
    [_adView addSubview:_iAd];*/
    
    _gAd = [[GADBannerView alloc] init];
    _gAd.frame              = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
    _gAd.adUnitID           = @"ca-app-pub-6347221915460502/4920594877";
    _gAd.rootViewController = self;
    _gAd.delegate           = self;
    
    
    GADRequest *gadRequest = [GADRequest request];
    gadRequest.testDevices = @[GAD_SIMULATOR_ID, DEVICE_TEST_1];
    
    [_gAd loadRequest:gadRequest];
    
    [_adView addSubview:_gAd];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [_adView removeFromSuperview];
    _adView = nil;
    _gAd = nil;
}

- (void)addButtons
{
    // Vars
    CGRect statusBarFrame       = [[UIApplication sharedApplication] statusBarFrame];
    float   viewWidth           = self.view.frame.size.width,
            lblUltimoPontoY     = self.navigationController.navigationBar.frame.size.height+statusBarFrame.size.height;
    
    // Label Ultimo Ponto
    _lblUltimoPonto                 = [[UILabel alloc] init];
    _lblUltimoPonto.frame           = CGRectMake(0,lblUltimoPontoY,viewWidth,23);
    _lblUltimoPonto.text            = @"  Último ponto registrado:";
    _lblUltimoPonto.font            = [UIFont fontWithName:@"Helvetica" size:14];
    _lblUltimoPonto.backgroundColor = [UIColor whiteColor];
    _lblUltimoPonto.textColor       = [UIColor appBlueColor];
    
    [self.view addSubview:_lblUltimoPonto];
    
    
    // Ultimo Ponto Registrado
    _lblUltimoPontoTexto                = [[UILabel alloc] init];
    _lblUltimoPontoTexto.frame          = CGRectMake(5,_lblUltimoPonto.frame.origin.y+_lblUltimoPonto.frame.size.height + 5,viewWidth-10,18);
    _lblUltimoPontoTexto.font           = [UIFont fontWithName:@"Helvetica" size:15];
    _lblUltimoPontoTexto.textColor      = [UIColor whiteColor];
    _lblUltimoPontoTexto.numberOfLines  = 1;
    _lblUltimoPontoTexto.lineBreakMode  = NSLineBreakByCharWrapping;

    [self.view addSubview:_lblUltimoPontoTexto];
    
    // Botao desfazer
    _btnDesfazer                    = [[UIButton alloc] init];
    _btnDesfazer.frame              = CGRectMake(_lblUltimoPontoTexto.frame.origin.x, (_lblUltimoPontoTexto.frame.origin.y + _lblUltimoPontoTexto.frame.size.height + 5),62,18);
    _btnDesfazer.titleLabel.font    = [UIFont fontWithName:@"Helvetica" size:15];
    
    [_btnDesfazer setTitle:@"Desfazer" forState:UIControlStateNormal];
    [_btnDesfazer setTitleColor:[UIColor appRedColor] forState:UIControlStateNormal];
    [_btnDesfazer setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [_btnDesfazer setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
    [_btnDesfazer addTarget:self action:@selector(btnDesfazerTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:_btnDesfazer];
    
    
    // Label horario
    _lblRelogio                 = [[UILabel alloc] init];
    _lblRelogio.frame           = CGRectMake(0, (_btnDesfazer.frame.origin.y + _btnDesfazer.frame.size.height + verticalSpace), viewWidth, 44);
    _lblRelogio.font            = [UIFont fontWithName:@"Helvetica" size:48];
    _lblRelogio.text            = @"00:00:00";
    _lblRelogio.textColor       = [UIColor whiteColor];
    _lblRelogio.textAlignment   = NSTextAlignmentCenter;
    
    [self.view addSubview:_lblRelogio];
    
    // Botao Marcar
    float btnMarcarHorarioWidth     = 100;
    float btnMarcarHorarioHeight    = 100;
    float btnMarcarHorarioX         = (viewWidth/2)-(btnMarcarHorarioWidth/2);
    float btnMarcarHorarioY         = _lblRelogio.frame.origin.y+_lblRelogio.frame.size.height + verticalSpace;
    
    _btnMarcarHorario                       = [[UIButton alloc] init];
    _btnMarcarHorario.frame                 = CGRectMake(btnMarcarHorarioX, btnMarcarHorarioY, btnMarcarHorarioWidth, btnMarcarHorarioHeight);
    _btnMarcarHorario.backgroundColor       = [UIColor appRedColor];
    _btnMarcarHorario.layer.cornerRadius    = 50;
    
    [_btnMarcarHorario addTarget:self action:@selector(changeButtonBackground:) forControlEvents:UIControlEventAllEvents];

    [_btnMarcarHorario setTitle:@"Marcar" forState:UIControlStateNormal];
    [_btnMarcarHorario setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnMarcarHorario setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [_btnMarcarHorario addTarget:self action:@selector(btnMarcarHorarioTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.btnMarcarHorario];
}


- (void)changeButtonBackground:(id)sender
{
    UIButton *btn = sender;
    if(btn && btn.state != UIControlStateNormal)
        [_btnMarcarHorario setBackgroundColor:[UIColor lightGrayColor]];
    else
        [_btnMarcarHorario setBackgroundColor:[UIColor appRedColor]];
}

- (void)customizeTableView
{

    if(!table) {
        table                   = [[UITableView alloc] init];
        table.rowHeight         = 40;
        table.backgroundColor   = self.view.backgroundColor;
        table.dataSource        = self;
        table.delegate          = self;
        table.allowsSelection   = NO;
        
        [self.view addSubview:table];
    }
    CGFloat tableY      = (_btnMarcarHorario.frame.origin.y + _btnMarcarHorario.frame.size.height) + verticalSpace;
    CGFloat tableHeight = self.navigationController.view.frame.size.height - tableY - (_adView.hidden ? 0 : _adView.frame.size.height);
    
    table.frame = CGRectMake(0, tableY, self.view.frame.size.width, tableHeight);
    
}


#pragma mark Carrega Dados
- (void)loadUltimoPontoRegistrado
{
    ultimoPontoRegistrado = [modelPonto getUltimoPontoAnterior];
    
    if(!ultimoPontoRegistrado)
        _btnDesfazer.alpha = 0;
    else
        _btnDesfazer.alpha = 1;
}


-(void)loadTableData:(BOOL)reloadTable
{
    tableData = [modelPonto getPontosWithLimit:maxTableItems];
    
    if(reloadTable)
        [table reloadData];
}


#pragma mark Tarefas NSTimer
- (void)timerTarefas:(NSTimer *)timer
{
    [self atualizaRelogio];
    [self atualizaTempoUltimoPonto];
}



- (void)atualizaRelogio
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    self.lblRelogio.text = [formatter stringFromDate:[[NSDate alloc] init]];
}


- (void)atualizaTempoUltimoPonto
{
    if(ultimoPontoRegistrado == nil) {
        _lblUltimoPontoTexto.text = @"Nenhum ponto registrado";
        return;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"HH:mm:ss"];
    
    // Calcula tempo decorrido desde ultimo ponto
    NSDate *dataUltimoPonto = [NSDate dateWithTimeIntervalSince1970:(double)ultimoPontoRegistrado.data];
    NSString *strSegs       = [NSDate formataStrSegundos:[[NSDate date] timeIntervalSinceDate:dataUltimoPonto]];
    NSString *strDecorridos = [NSString stringWithFormat:@"%@ decorridos",strSegs];
    
    [formatter setDateFormat:dateFormatComplete];
    NSString *strHorario = [formatter stringFromDate:dataUltimoPonto];
    
    // Altera Texto da Label
    _lblUltimoPontoTexto.text = [NSString stringWithFormat:@"%@ (%@)",strHorario, strDecorridos];
}


#pragma mark - Buttons Actions
- (void)btnDesfazerTouched:(id)sender {
    // Exclui ultimo ponto
    [modelPonto excluirPonto:ultimoPontoRegistrado];

    [self fadeOutUltimoPonto];
    [self fadeOutTabela];
}

- (void)btnMarcarHorarioTouched:(id)sender
{
    [self changeButtonBackground:nil];
    // Marca um ponto novo
    Ponto *novoPonto = [[Ponto alloc] init];
    novoPonto.data = [[[NSDate alloc] init] timeIntervalSince1970];
    
    [modelPonto salvarPonto:novoPonto];
    
    [self fadeOutUltimoPonto];
    [self fadeOutTabela];
}


#pragma mark Animations
- (void)fadeOutTabela
{
    [UIView beginAnimations:@"fade-tabela" context:nil];
    [UIView setAnimationDuration:0.2];
    [UIView setAnimationDelegate:self];
    
    UITableViewCell *cell = [table cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    cell.alpha = 0;
    
    [UIView commitAnimations];
}


- (void)fadeInTabela
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.2];
    
    [self loadTableData:YES];
    UITableViewCell *cell = [table cellForRowAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0]];
    cell.alpha = 1;
    
    [UIView commitAnimations];
}


- (void)fadeOutUltimoPonto
{
    [self loadUltimoPontoRegistrado];
    [UIView beginAnimations:@"fade-ultimo-ponto" context:nil];
    [UIView setAnimationDelegate:self];
    
    [UIView setAnimationDuration:0.3];
    
    _lblUltimoPontoTexto.alpha = 0;
    
    [UIView commitAnimations];
}

- (void)fadeInUltimoPonto
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    [self atualizaTempoUltimoPonto];
    _lblUltimoPontoTexto.alpha = 1;
    
    [UIView commitAnimations];
}

#pragma mark - Animation Delegate
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    if([anim.description isEqualToString:@"fade-ultimo-ponto"])
        [self fadeInUltimoPonto];
    else if([anim.description isEqualToString:@"fade-tabela"])
        [self fadeInTabela];
}

#pragma mark - Table View Data Source

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Últimos registros";
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return tableData.count;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdent = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdent];
    
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdent];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor lightTextColor];
    }
    
    Ponto *p = [tableData objectAtIndex:indexPath.row];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:dateFormatComplete];
    
    cell.textLabel.text = [formatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:p.data]];
    
    return cell;
}


#pragma mark - Table View Delegate
- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section
{
    UITableViewHeaderFooterView *header = (UITableViewHeaderFooterView *)view;
    header.textLabel.textColor = self.view.backgroundColor;
    header.textLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    header.contentView.backgroundColor = [UIColor whiteColor];
}


#pragma mark Ad Banner View Delegate
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    /*
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _iAd.frame = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _iAd.hidden = YES;
    [self customizeTableView];
    
    [UIView commitAnimations];
     */
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    /*
    if(_iAd.hidden) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        _iAd.hidden             = NO;
        _iAd.frame              = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50);
        [self customizeTableView];
        
        [UIView commitAnimations];
    }
    
    if(!_iAd.superview) {
        [self.view addSubview:_iAd];
    }
    */
}

- (void)bannerViewWillLoadAd:(ADBannerView *)banner
{
    
}


#pragma mark Google Ad View Banner Delegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    [self showAdView];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    [self hideAdView];
}


- (void)showAdView
{
    if(!_adView.superview)
        [self.view addSubview:_adView];
    
    if(_adView.hidden) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        _adView.hidden             = NO;
        _adView.frame              = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50);
        _adView.backgroundColor = [UIColor redColor];
        
        [self customizeTableView];
        
        [UIView commitAnimations];
    }
}


- (void)hideAdView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _adView.frame = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden = YES;
    [self customizeTableView];
    
    [UIView commitAnimations];
}

@end
