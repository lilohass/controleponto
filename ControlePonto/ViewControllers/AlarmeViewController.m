//
//  AlarmeViewController.m
//  ControlePonto
//
//  Created by William Hass on 3/22/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "AlarmeViewController.h"
#import <AVFoundation/AVFoundation.h>

typedef enum {
    TimerStatusPausado,
    TimerStatusIniciado,
    TimerStatusInicial
    
} TimerStatus;

@interface AlarmeViewController () {
    UIDatePicker *_datePicker;
    UIButton *_btnIniciarCancelar;
    UIButton *_btnPausarRetormar;
    UILabel *_lblTimer;
    AVAudioPlayer *_player;
    
    NSTimeInterval _contagemTempo;
    
    NSTimer *_timer;
    
    NSDate *_dataInicial;
    NSDate *_dataFinal;
    
    NSDateFormatter *_formatter;
    UILocalNotification *_localNotification;
    NSURL *_soundFileURL;
    
    SystemSoundID soundID;
    TimerStatus statusAtual;
    
    BOOL canAlarm;
    
    BOOL alarmHasInterrupts;
    
    //ADBannerView *_iAd;
    GADBannerView *_gAd;
    UIView *_adView;
}


@end

@implementation AlarmeViewController

static NSString *soundFile = @"/System/Library/Audio/UISounds/alarm.caf";

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    canAlarm = YES;

    // Toolbar
    [self.navigationController setToolbarHidden:YES animated:YES];
    
    _adView         = [[UIView alloc] init];
    _adView.frame   = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden  = YES;
    
    /* iAd
    _iAd            = [[ADBannerView alloc] init];
     _iAd.delegate   = self;
     _iAd.hidden     = YES;
     _iAd.frame      = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
     
     [_adView addSubview:_iAd];*/
    
    _gAd = [[GADBannerView alloc] init];
    _gAd.frame              = CGRectMake(0, 0, _adView.frame.size.width, _adView.frame.size.height);
    _gAd.adUnitID           = @"ca-app-pub-6347221915460502/4220760877";
    _gAd.rootViewController = self;
    _gAd.delegate           = self;
    
    
    GADRequest *gadRequest = [GADRequest request];
    gadRequest.testDevices = @[GAD_SIMULATOR_ID, DEVICE_TEST_1];
    
    [_gAd loadRequest:gadRequest];
    
    [_adView addSubview:_gAd];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [_adView removeFromSuperview];
    _adView = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"Alarme";

    statusAtual = TimerStatusInicial;
    
    alarmHasInterrupts = NO;
    
    self.view.backgroundColor = [UIColor appBlueColor];
    
    [self addElements];
    
    _soundFileURL = [NSURL URLWithString:soundFile];
    _player = [[AVAudioPlayer alloc] initWithContentsOfURL:_soundFileURL error:nil];
    _player.numberOfLoops = 30;
    
    _formatter = [[NSDateFormatter alloc] init];
    [_formatter setDateFormat:@"HH:mm"];
    
    // Verifica Notificacao
    NSArray *notificacoes = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for(UILocalNotification *notif in notificacoes) {
        if([notif.userInfo objectForKey:@"controle-ponto"] != nil) {
            
            _dataFinal = notif.fireDate;
            statusAtual = TimerStatusIniciado;
            _localNotification = notif;
            _datePicker.hidden = YES;
            _lblTimer.hidden = NO;
            
            [self setBtnsStatus];
            [self iniciarTimer];
            
            break;
        }
    }
}


- (void)addElements
{
    // Date Picker
    _datePicker = [[UIDatePicker alloc] init];
    _datePicker.locale = [NSLocale localeWithLocaleIdentifier:@"pt-BR"];
    _datePicker.frame = CGRectMake(0,
                                   self.navigationController.navigationBar.frame.size.height + [[UIApplication sharedApplication] statusBarFrame].size.height,
                                   0, 0);
    _datePicker.datePickerMode = UIDatePickerModeCountDownTimer;
    
    [self.view addSubview:_datePicker];
    
    
    // Label Timer
    _lblTimer = [[UILabel alloc] init];
    _lblTimer.text = @"00:00:00";
    _lblTimer.font = [UIFont fontWithName:@"Helvetica Light" size:60];
    _lblTimer.hidden = YES;
    _lblTimer.textColor = [UIColor lightTextColor];
    _lblTimer.textAlignment = NSTextAlignmentCenter;
    _lblTimer.frame = CGRectMake(0,
                                 _datePicker.frame.origin.y + (_datePicker.frame.size.height/2) - 30,
                                 _datePicker.frame.size.width,
                                 60);
    
    
    [self.view addSubview:_lblTimer];
    
    // Btn IniciarCancelar
    _btnIniciarCancelar = [[UIButton alloc] init];
    _btnIniciarCancelar.frame = CGRectMake( (self.view.frame.size.width/2) - ((self.view.frame.size.width/2) - 35),
                                           _datePicker.frame.origin.y+_datePicker.frame.size.height + 20,
                                           70,
                                           70);
    _btnIniciarCancelar.layer.cornerRadius =  _btnIniciarCancelar.frame.size.width/2;
    
    _btnIniciarCancelar.titleLabel.textColor = [UIColor lightTextColor];
    [_btnIniciarCancelar setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    [_btnIniciarCancelar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnIniciarCancelar addTarget:self action:@selector(changeButtonBackground:) forControlEvents:UIControlEventAllEvents];
    [_btnIniciarCancelar addTarget:self action:@selector(btnIniciarCancelarTouched:) forControlEvents:UIControlEventTouchUpInside];
    _btnIniciarCancelar.titleLabel.font = [UIFont fontWithName:@"Helvetica Light" size:14];
    
    [self setBtnIniciarCancelarStatus];
    
    [self.view addSubview:_btnIniciarCancelar];
    
    // Btn Pausar Retomar
    _btnPausarRetormar = [[UIButton alloc] init];
    _btnPausarRetormar.frame = CGRectMake((self.view.frame.size.width/2) + ((self.view.frame.size.width/2)/2 - 35),
                                          _btnIniciarCancelar.frame.origin.y,
                                          70,
                                          70);
    _btnPausarRetormar.layer.cornerRadius = _btnPausarRetormar.frame.size.width/2;
    [_btnPausarRetormar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [_btnPausarRetormar setTitleColor:[UIColor lightTextColor] forState:UIControlStateHighlighted];
    _btnPausarRetormar.backgroundColor = [UIColor lightGrayColor];
    _btnPausarRetormar.titleLabel.font = [UIFont fontWithName:@"Helvetica Light" size:14];
    [_btnPausarRetormar addTarget:self action:@selector(changeButtonBackground:) forControlEvents:UIControlEventAllEvents];
    [_btnPausarRetormar addTarget:self action:@selector(btnPausarRetomarTouched:) forControlEvents:UIControlEventTouchUpInside];
    
    [self setBtnPausarRetomarStatus];
    
    [self.view addSubview:_btnPausarRetormar];
}


- (void)changeButtonBackground:(id)sender
{
    UIButton *btn = sender;
    if(btn && btn.state != UIControlStateNormal) {
        [btn setBackgroundColor:[UIColor darkGrayColor]];
    } else {
        [self setBtnsStatus];
    }
}



- (void)setBtnPausarRetomarStatus
{
    _btnPausarRetormar.backgroundColor = [UIColor lightGrayColor];
    if(statusAtual == TimerStatusInicial)
        _btnPausarRetormar.enabled = NO;
    else
        _btnPausarRetormar.enabled = YES;
    
    if(statusAtual != TimerStatusPausado)
        [_btnPausarRetormar setTitle:@"Pausar" forState:UIControlStateNormal];
    else
        [_btnPausarRetormar setTitle:@"Retomar" forState:UIControlStateNormal];
    
}



- (void) setBtnIniciarCancelarStatus
{
    if(statusAtual == TimerStatusInicial) {
        _btnIniciarCancelar.backgroundColor = [UIColor greenColor];
        [_btnIniciarCancelar setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_btnIniciarCancelar setTitle:@"Iniciar" forState:UIControlStateNormal];
    } else {
        _btnIniciarCancelar.backgroundColor = [UIColor appRedColor];
        [_btnIniciarCancelar setTitleColor:[UIColor lightTextColor] forState:UIControlStateNormal];
        [_btnIniciarCancelar setTitle:@"Cancelar" forState:UIControlStateNormal];
    }
    
}

- (void)setBtnsStatus
{
    [self setBtnIniciarCancelarStatus];
    [self setBtnPausarRetomarStatus];
}

- (void)doIniciar:(NSTimeInterval)duration
{
    statusAtual = TimerStatusIniciado;
    _datePicker.hidden = YES;
    _lblTimer.hidden = NO;
    
    _dataFinal      = [NSDate dateWithTimeIntervalSinceNow:duration];
    
    [self iniciarTimer];
    [self setBtnsStatus];
    [self registrarNotificacao];
}


- (void)doRetomar
{
    statusAtual = TimerStatusIniciado;
    
    _dataFinal      = [NSDate dateWithTimeIntervalSinceNow:_contagemTempo];
    
    [self iniciarTimer];
    [self setBtnsStatus];
    [self registrarNotificacao];
}

- (void)doCancelar
{
    statusAtual = TimerStatusInicial;
    _datePicker.hidden = NO;
    _lblTimer.hidden = YES;
    
    _contagemTempo = 0;
    
    [[UIApplication sharedApplication] cancelLocalNotification:_localNotification];
    [_timer invalidate];
    [self setBtnsStatus];
}

- (void)doPausar
{
    statusAtual = TimerStatusPausado;
    
    [[UIApplication sharedApplication] cancelLocalNotification:_localNotification];
    [_timer invalidate];
    [self setBtnsStatus];
}


- (void) iniciarTimer
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(timerAction:) userInfo:nil repeats:YES];
    _contagemTempo = [_dataFinal timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    [self updateLblTimer];
    //[self timerAction:_timer];
}



- (void)registrarNotificacao
{
    if(!_localNotification) {
        _localNotification = [[UILocalNotification alloc] init];
        _localNotification.timeZone = [NSTimeZone defaultTimeZone];
        _localNotification.hasAction = YES;
        _localNotification.repeatInterval = 0;
        _localNotification.userInfo = [NSDictionary dictionaryWithObject:@"controle-ponto" forKey:@"controle-ponto"];
        _localNotification.soundName = @"alarm.caf";
    }
    
    _localNotification.alertBody = [NSString stringWithFormat:@"Hora agora: %@",[_formatter stringFromDate:_dataFinal]];
    _localNotification.fireDate = _dataFinal;
    [[UIApplication sharedApplication] scheduleLocalNotification:_localNotification];
}




- (void)updateLblTimer
{
    
    _lblTimer.text = [NSDate formataStrSegundos:_contagemTempo];
}


- (void)timerAction:(NSTimer *)timer
{
    _contagemTempo = [_dataFinal timeIntervalSince1970] - [[NSDate date] timeIntervalSince1970];
    
    // Acabou
    if(_contagemTempo <= 0) {
        
        if([[UIApplication sharedApplication] applicationState] == UIApplicationStateActive && canAlarm)
            [self doAlarm];
        else if(!canAlarm)
            alarmHasInterrupts = YES;
        
        [self doCancelar];
    }
    
    [self updateLblTimer];
}

#pragma mark Sounds
- (void)playAlarm
{
    [_player play];
}

- (void)stopAlarm
{
    [_player stop];
}


- (void)doAlarm
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Seu contador chegou ao fim"
                                                    message:[NSString stringWithFormat:@"Hora agora: %@",[_formatter stringFromDate:[NSDate date]]]
                                                   delegate:self
                                          cancelButtonTitle:@"Ok"
                                          otherButtonTitles:nil];
    [self playAlarm];
    [alert show];

}

#pragma mark - Button Actions

- (void)btnIniciarCancelarTouched:(id)sender
{
    // Cancelar
    if(statusAtual != TimerStatusInicial)
        [self doCancelar];
    // Iniciar
    else
        [self doIniciar:[_datePicker countDownDuration]];
}


- (void)btnPausarRetomarTouched:(id)sender
{
    if(statusAtual == TimerStatusIniciado)
        [self doPausar];
    else if(statusAtual == TimerStatusPausado)
        [self doRetomar];
}


#pragma mark - Alert View Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    [self stopAlarm];
}


#pragma mark Ad Banner View Delegate
- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    if(!willLeave)
        canAlarm = NO;
    return YES;
}

- (void)bannerViewActionDidFinish:(ADBannerView *)banner
{
    if(alarmHasInterrupts) {
        alarmHasInterrupts = NO;
        [self doAlarm];
    }
}

/*- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _iAd.frame  = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _iAd.hidden = YES;
    
    [UIView commitAnimations];
}

- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    if(!_iAd.superview)
        [self.view addSubview:_iAd];
    
    if(_iAd.hidden) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        _iAd.hidden             = NO;
        _iAd.frame              = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50);
        
        [UIView commitAnimations];
    }
    
}


- (void)bannerViewWillLoadAd:(ADBannerView *)banner
{
    
}*/

#pragma mark Google Ad View Banner Delegate
- (void)adViewDidReceiveAd:(GADBannerView *)view
{
    [self showAdView];
}

- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error
{
    [self hideAdView];
}


- (void)showAdView
{
    if(!_adView.superview)
        [self.view addSubview:_adView];
    
    if(_adView.hidden) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        
        _adView.hidden             = NO;
        _adView.frame              = CGRectMake(0, self.view.frame.size.height-50, self.view.frame.size.width, 50);
        _adView.backgroundColor = [UIColor redColor];
        
        [UIView commitAnimations];
    }
}


- (void)hideAdView
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3];
    
    _adView.frame = CGRectMake(0, self.view.frame.size.height+10, self.view.frame.size.width, 50);
    _adView.hidden = YES;
    
    [UIView commitAnimations];
}

@end