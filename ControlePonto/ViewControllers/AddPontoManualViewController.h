//
//  AddPontoManualViewController.h
//  ControlePonto
//
//  Created by William Hass on 3/22/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddPontoManualViewController : UIViewController <ADBannerViewDelegate, GADBannerViewDelegate>

@end
