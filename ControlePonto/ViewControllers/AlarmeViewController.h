//
//  AlarmeViewController.h
//  ControlePonto
//
//  Created by William Hass on 3/22/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmeViewController : UIViewController <UIAlertViewDelegate, ADBannerViewDelegate, GADBannerViewDelegate>

- (void)btnIniciarCancelarTouched:(id)sender;
- (void)btnPausarRetomarTouched:(id)sender;
- (void)timerAction:(NSTimer *)timer;

@end
