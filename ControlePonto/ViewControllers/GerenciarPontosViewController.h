//
//  GerenciarPontosViewController.h
//  ControlePonto
//
//  Created by William Hass on 3/4/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PontoModel.h"
@interface GerenciarPontosViewController : UITableViewController <UIActionSheetDelegate, ADBannerViewDelegate, GADBannerViewDelegate>

@property PontoModel *modelPonto;
@property NSArray *tableData;

- (void)btnAgruparTouched:(id)sender;

@end
