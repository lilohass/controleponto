//
//  LeftPanelViewController.m
//  ControlePonto
//
//  Created by William Hass on 2/25/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "LeftPanelViewController.h"
#import "GerenciarPontosViewController.h"
#import "InicioViewController.h"
#import "AddPontoManualViewController.h"
#import "AlarmeViewController.h"

@interface LeftPanelViewController ()
{
    UITableView *table;
    NSArray *_tableData;
    
    UINavigationController *_nav;
    
    InicioViewController *_inicioVc;
    GerenciarPontosViewController *_gpVc;
    AddPontoManualViewController *_addVc;
    AlarmeViewController *_alarmeVc;
}
@end

@implementation LeftPanelViewController


- (void)setNav:(UINavigationController *)nav
{
    _nav = nav;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _tableData = [NSArray arrayWithObjects:@"Inicio",@"Gerenciar pontos",@"Inserir ponto manual",@"Alarme",nil];
    
    [self configureTable];
    self.view.backgroundColor = [UIColor darkGrayColor];
}


- (void)configureTable
{
    table = [[UITableView alloc] initWithFrame:CGRectMake(0, 20, self.view.frame.size.width, self.view.frame.size.height)];
    table.backgroundColor = [UIColor clearColor];
    table.separatorInset = UIEdgeInsetsZero;
    table.dataSource = self;
    table.delegate = self;
    [self.view addSubview:table];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table View Data Source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _tableData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseIdentifier = @"Cell";
    
    UITableViewCell *cell = [table dequeueReusableCellWithIdentifier:reuseIdentifier];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        
        cell.textLabel.textColor = [UIColor lightTextColor];
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:16];
        
    }
    
    cell.textLabel.text = [_tableData objectAtIndex:indexPath.row];
    
    return cell;
    
}



#pragma mark Table View Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    JASidePanelController *panel = [self sidePanelController];

    // Inicio
    if(indexPath.row == 0) {
        if(!_inicioVc)
            _inicioVc = [[InicioViewController alloc] init];
        [_nav setViewControllers:[NSArray arrayWithObject:_inicioVc]];
    }
    // Gerenciar pontos
    else if(indexPath.row == 1) {
        if(!_gpVc)
            _gpVc = [[GerenciarPontosViewController alloc] init];
        [_nav setViewControllers:[NSArray arrayWithObject:_gpVc]];
    }
    // Inserir ponto manual
    else if(indexPath.row == 2) {
        if(!_addVc)
            _addVc = [[AddPontoManualViewController alloc] init];
        [_nav setViewControllers:[NSArray arrayWithObject:_addVc]];
    }
    // Alarme
    else if(indexPath.row == 3) {
        if(!_alarmeVc)
            _alarmeVc = [[AlarmeViewController alloc] init];
        [_nav setViewControllers:[NSArray arrayWithObject:_alarmeVc]];
    }
    
    [panel setCenterPanel:_nav];
    
}

@end
