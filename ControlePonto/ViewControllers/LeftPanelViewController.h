//
//  LeftPanelViewController.h
//  ControlePonto
//
//  Created by William Hass on 2/25/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+JASidePanel.h"
#import "JASidePanelController.h"

@interface LeftPanelViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

- (void)setNav:(UINavigationController *)nav;

@end
