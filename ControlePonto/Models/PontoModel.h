//
//  PontoModel.h
//  ControlePonto
//
//  Created by William Hass on 2/23/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "AppModel.h"
#import "Ponto.h"

@interface PontoModel : AppModel

- (Ponto *)getUltimoPontoAnterior;
- (BOOL)salvarPonto:(Ponto *)ponto;
- (BOOL)excluirPonto:(Ponto *)ponto;
- (NSArray *)getPontosWithLimit:(int)limit;
- (NSArray *)getPontos;
- (NSArray *)getPontosWithOrder:(NSString *)ordem;

@end
