//
//  AppModel.h
//  ControlePonto
//
//  Created by William Hass on 2/23/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDatabase.h"
@interface AppModel : NSObject

@property FMDatabase *database;
@property BOOL debug;

- (id) initWithDebug:(BOOL)debug;

@end
