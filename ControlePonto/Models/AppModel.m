//
//  AppModel.m
//  ControlePonto
//
//  Created by William Hass on 2/23/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "AppModel.h"

@implementation AppModel

static FMDatabase *_database;
static NSString *nomeArquivoBanco = @"database";

- (id) init
{
    self = [super init];
    if(self) {
        [self start];
    }
    return self;
}

+ (FMDatabase *)getDb
{
    return _database;
}

+ (void) setDb:(FMDatabase *)db
{
    _database = db;
}

- (id) initWithDebug:(BOOL)debug
{
    self.debug = debug;
    return [self init];
}

- (BOOL) criarBase
{
    BOOL retorno = YES;

    // Tabela ponto
    retorno = [self.database executeUpdate:@"CREATE TABLE ponto("
               @"id_ponto INTEGER PRIMARY KEY,"
               @"id_marcador INTEGER,"
               @"data INTEGER,"
               @"data_offset INTEGER"
               @")"];
    
    if(!retorno) {
        NSLog(@"Erro ao criar a tabela convidado");
        return retorno;
    }
    
    return retorno;
}

- (void) start
{
    self.database = [AppModel getDb];
    
    if(self.database == nil) {
        NSString *docPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
        NSString *dbPath = [docPath stringByAppendingPathComponent:nomeArquivoBanco];
        
        if(self.debug)
            NSLog(@"Caminho do banco: %@",dbPath);
        
        //[[NSFileManager defaultManager] removeItemAtPath:dbPath error:nil];
        BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:dbPath];
        
        self.database = [FMDatabase databaseWithPath:dbPath];
        
        if(self.database == nil)
            NSLog(@"Erro ao iniciar o banco de dados");
        
        BOOL abriu = [self.database open];
        
        if(!fileExists && abriu)
            [self criarBase];
        else if(!abriu)
            NSLog(@"Erro ao abrir o banco de dados");
        
        [AppModel setDb:self.database];
    }
}

@end
