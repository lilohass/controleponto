//
//  Ponto.h
//  ControlePonto
//
//  Created by William Hass on 2/23/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Ponto : NSObject

@property int id_ponto;
@property int data;
@property int data_offset;

@end
