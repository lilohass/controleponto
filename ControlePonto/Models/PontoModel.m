//
//  PontoModel.m
//  ControlePonto
//
//  Created by William Hass on 2/23/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "PontoModel.h"

@implementation PontoModel

static NSString *tabela = @"ponto";

- (Ponto *)fillResult:(FMResultSet *)result
{
    Ponto *p;
    
    p = [[Ponto alloc] init];
    p.id_ponto      = [result intForColumn:@"id_ponto"];
    p.data          = [result intForColumn:@"data"];
    p.data_offset   = [result intForColumn:@"data_offset"];

    return p;
}

- (Ponto *)getUltimoPontoAnterior
{
    int inicio_dia  = [NSDate timestampInicioDia];
    int agora       = [[NSDate date] timeIntervalSince1970];
    
    NSString *sql = [NSString stringWithFormat: @"SELECT * FROM %@"
                                                @" WHERE data BETWEEN %d AND %d " // Entre inicio do dia e agora
                                                @" ORDER BY data DESC LIMIT 1 ",tabela, inicio_dia, agora]; // Traz o ultimo
    
    FMResultSet *result = [self.database executeQuery:sql];
    
    Ponto *p;
    
    if([result next])
        p = [self fillResult:result];
    
    return p;
    
}

- (NSArray *)getPontosWithLimit:(int)limit
{
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY data DESC LIMIT %d",tabela,limit];
    
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    FMResultSet *result = [self.database executeQuery:sql];
    
    while([result next]) {
        Ponto *p = [self fillResult:result];
        
        [ret addObject:p];
    }
    
    return (NSArray *)ret;
}


- (NSArray *)getPontos
{
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY data DESC ",tabela];
    
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    FMResultSet *result = [self.database executeQuery:sql];
    
    while([result next]) {
        Ponto *p = [self fillResult:result];
        
        [ret addObject:p];
    }
    
    return (NSArray *)ret;
}


- (NSArray *)getPontosWithOrder:(NSString *)ordem
{
    NSString *sql = [NSString stringWithFormat:@"SELECT * FROM %@ ORDER BY data %@ ",tabela,ordem];
    
    NSMutableArray *ret = [[NSMutableArray alloc] init];
    
    FMResultSet *result = [self.database executeQuery:sql];
    
    while([result next]) {
        Ponto *p = [self fillResult:result];
        
        [ret addObject:p];
    }
    
    return (NSArray *)ret;
}

- (BOOL)salvarPonto:(Ponto *)ponto
{
    NSString *sql = [NSString stringWithFormat:@"INSERT INTO %@ (data) VALUES (%d)",tabela,ponto.data];
    
    return [self.database executeUpdate:sql];
    
}

- (BOOL)excluirPonto:(Ponto *)ponto
{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE id_ponto = %d",tabela,ponto.id_ponto];
    return [self.database executeUpdate:sql];
}

@end
