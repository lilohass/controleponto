//
//  UIColor+UIColorCategory.m
//  ControlePonto
//
//  Created by William Hass on 3/17/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "UIColor+UIColorCategory.h"

@implementation UIColor (UIColorCategory)

+ (UIColor *)appBlueColor
{
    return [UIColor colorWithRed:0x26/255. green:0x97/255. blue:0xE1/255. alpha:1];
}

+ (UIColor *)appRedColor
{
    return [UIColor colorWithRed:0x230/255. green:0x63/255. blue:0x62/255. alpha:1];
}

+ (UIColor *)appGreenColor
{
    return [UIColor colorWithRed:0x00/255. green:0xFF/255. blue:0x91/255. alpha:1];
}
@end





































