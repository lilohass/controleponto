//
//  UIImage+UIImageCategory.h
//  ControlePonto
//
//  Created by William Hass on 3/4/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImageCategory)

+ (UIImage *)customNavigationImageButton;
+ (UIImage *)imageWithColor:(UIColor *)color;

@end
