//
//  UIColor+UIColorCategory.h
//  ControlePonto
//
//  Created by William Hass on 3/17/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (UIColorCategory)

+ (UIColor *)appBlueColor;
+ (UIColor *)appRedColor;
+ (UIColor *)appGreenColor;

@end
