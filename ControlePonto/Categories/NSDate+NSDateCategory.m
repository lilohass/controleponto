//
//  NSDate+NSDateCategory.m
//  ControlePonto
//
//  Created by William Hass on 2/23/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import "NSDate+NSDateCategory.h"

@implementation NSDate (NSDateCategory)

+ (int)timestampInicioDia
{
    NSDate *date = [[NSDate alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd 00:00:00 xxxx"];
    
    NSDate *data_nova   = [formatter dateFromString:[formatter stringFromDate:date]];
    
//    int curr_stamp  = [date timeIntervalSince1970];
//    int new_stamp   = [data_nova timeIntervalSince1970];
    
    return [[NSNumber numberWithDouble:[date timeIntervalSinceDate:data_nova]] intValue];
}


+ (NSString *)formataStrSegundos:(NSTimeInterval)segundos
{
    int segundosPassados    = (int)segundos%60;
    int minutosPassados     = ((int)segundos/60)%60;
    int horasPassados       = (int)(segundos/60)/60;
    
    NSString *strSegundos = [NSString stringWithFormat:(segundosPassados > 9 ? @"%d" : @"0%d"),segundosPassados];
    NSString *strMinutos = [NSString stringWithFormat:(minutosPassados > 9 ? @"%d" : @"0%d"),minutosPassados];
    NSString *strHoras = [NSString stringWithFormat:(horasPassados > 9 ? @"%d" : @"0%d"),horasPassados];
    
    return [NSString stringWithFormat:@"%@:%@:%@",strHoras, strMinutos, strSegundos];
}

@end
