//
//  NSDate+NSDateCategory.h
//  ControlePonto
//
//  Created by William Hass on 2/23/14.
//  Copyright (c) 2014 William Hass. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (NSDateCategory)

+ (int) timestampInicioDia;
+ (NSString *)formataStrSegundos:(NSTimeInterval)segundos;
@end
